package carfora.matteo.tweb;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.util.ArrayList;

import carfora.matteo.tweb.adapters.ListaPizzeAdapter;
import carfora.matteo.tweb.asynctask.RequestData;
import carfora.matteo.tweb.model.Pizza;

public class ListaPizzeActivity extends AppCompatActivity implements ICallBack{
    private ListView listaPizza;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pizze_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listaPizza = (ListView)findViewById(R.id.listaPizza);
        RequestData requestData = new RequestData(this);
        requestData.execute("get",Common.urlApi+"/pizze");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void taskStart() {

    }

    @Override
    public void taskCompleted(Object result) {
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonArray array = parser.parse((String)result).getAsJsonArray();
        ArrayList<Pizza> pizze = new ArrayList<>();
        for(int i=0;i<array.size();i++) {
            Pizza pizza = gson.fromJson(array.get(i), Pizza.class);
            pizze.add(pizza);
        }
        ListaPizzeAdapter adapter = new ListaPizzeAdapter(this,pizze);
        listaPizza.setAdapter(adapter);
    }

    @Override
    public void taskProgress(Object[] progress) {

    }

    @Override
    public void taskCancelled(int val, int size) {

    }
}
