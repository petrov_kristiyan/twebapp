package carfora.matteo.tweb;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.util.ArrayList;

import carfora.matteo.tweb.adapters.ListaPrenotazioniAdapter;
import carfora.matteo.tweb.asynctask.RequestData;
import carfora.matteo.tweb.model.Prenotazione;

public class PrenotazioniActivity extends AppCompatActivity implements ICallBack {
    private ListView listaPrenotazioni;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prenotazioni);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listaPrenotazioni = (ListView) findViewById(R.id.listaPrenotazioni);
        RequestData requestData = new RequestData(this);
        requestData.execute("get",Common.urlApi+"/prenotazioni");
    }

    @Override
    public void taskStart() {

    }

    @Override
    public void taskCompleted(Object result) {
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonArray array = parser.parse((String)result).getAsJsonArray();
        ArrayList<Prenotazione> prenotazioni = new ArrayList<>();
        for(int i=0;i<array.size();i++) {
            Prenotazione prenotazione = gson.fromJson(array.get(i), Prenotazione.class);
            prenotazioni.add(prenotazione);
        }
        ListaPrenotazioniAdapter listaPrenotazioniAdapter = new ListaPrenotazioniAdapter(this,prenotazioni);
        listaPrenotazioni.setAdapter(listaPrenotazioniAdapter);
        listaPrenotazioni.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> myAdapter, View myView, int myItemInt, long mylng) {
                Intent intent = new Intent(PrenotazioniActivity.this, PrenotazioneActivity.class);
                intent.putExtra("prenotazione",(Prenotazione) listaPrenotazioni.getItemAtPosition(myItemInt));
                PrenotazioniActivity.this.startActivity(intent);
            }
        });
    }

    @Override
    public void taskProgress(Object[] progress) {

    }

    @Override
    public void taskCancelled(int val, int size) {

    }
}
