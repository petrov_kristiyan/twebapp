package carfora.matteo.tweb;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import carfora.matteo.tweb.asynctask.RequestData;
import carfora.matteo.tweb.model.User;

public class LoginActivity extends AppCompatActivity implements ICallBack {

    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final EditText username = (EditText) findViewById(R.id.username);
        final EditText password = (EditText) findViewById(R.id.password);
        Button loginButton = (Button) findViewById(R.id.login);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Gson gson = new Gson();
                RequestData requestData = new RequestData(LoginActivity.this);
                String json = gson.toJson(new User(username.getText().toString(), password.getText().toString()));
                requestData.execute("post",Common.urlBase + "/login",json);
            }
        });


        //post
    }

    @Override
    public void taskStart() {

    }

    @Override
    public void taskCompleted(Object result) {
       if(result.equals("OK")){
           Intent intent = new Intent(LoginActivity.this,MainActivity.class);
            startActivity(intent);
       }
       else {
          Toast.makeText(getApplicationContext(), "Errore di login", Toast.LENGTH_SHORT).show();
       }
    }

    @Override
    public void taskProgress(Object[] progress) {

    }

    @Override
    public void taskCancelled(int val, int size) {

    }
}
