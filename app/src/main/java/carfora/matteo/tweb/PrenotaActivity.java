package carfora.matteo.tweb;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import carfora.matteo.tweb.adapters.ListaPrenotaAdapter;
import carfora.matteo.tweb.asynctask.RequestData;
import carfora.matteo.tweb.model.Pizza;
import carfora.matteo.tweb.model.Prenota;
import carfora.matteo.tweb.model.Prenotazione;

public class PrenotaActivity extends AppCompatActivity implements ICallBack {
    private ListView listaPrenota;
    private ArrayList<Prenota> listaPrenotaPizze;
    private Prenotazione prenotazione = new Prenotazione();
    private int yearS,dayS,monthS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prenota);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        listaPrenota = (ListView) findViewById(R.id.listaPrenota);
        RequestData requestData = new RequestData(new ICallBack<String, String>() {
            @Override
            public void taskStart() {

            }

            @Override
            public void taskCompleted(String result) {
                if(result.isEmpty()){
                    Toast.makeText(getApplicationContext(), "Errore ricezione pizze", Toast.LENGTH_SHORT).show();
                    return;
                }
                Gson gson = new Gson();
                JsonParser parser = new JsonParser();
                JsonArray array = parser.parse((String) result).getAsJsonArray();
                listaPrenotaPizze = new ArrayList<>();
                for (int i = 0; i < array.size(); i++) {
                    Pizza pizza = gson.fromJson(array.get(i), Pizza.class);
                    Prenota prenota = new Prenota(pizza, 0);
                    listaPrenotaPizze.add(prenota);
                }
                ListaPrenotaAdapter listaPrenotaAdapter = new ListaPrenotaAdapter(PrenotaActivity.this, listaPrenotaPizze);
                listaPrenota.setAdapter(listaPrenotaAdapter);
            }

            @Override
            public void taskProgress(String... progress) {

            }

            @Override
            public void taskCancelled(int val, int size) {

            }
        });
        requestData.execute("get", Common.urlApi + "/pizze");


        Button prenota = (Button) findViewById(R.id.prenota);
        prenota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, Integer> pizze = new HashMap<>();
                for (Prenota prenota : listaPrenotaPizze) {
                    if (prenota.getQuantità() != 0)
                        pizze.put(prenota.getPizza().getNome(), prenota.getQuantità());
                }
                if(pizze.size()>0) {
                    DialogFragment newFragment = new DatePickerFragment();
                    newFragment.show(getSupportFragmentManager(), "datePicker");
                    prenotazione.setPizze(pizze);
                }

            }
        });
    }


    @Override
    public void taskStart() {

    }

    @Override
    public void taskCompleted(Object result) {
        if(((String)result).isEmpty()){
            Toast.makeText(getApplicationContext(), "Errore di prenotazione, controlla la data e riprova", Toast.LENGTH_SHORT).show();

        }
        else{
            Toast.makeText(getApplicationContext(), "Prenotazione effettuata con successo!!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(PrenotaActivity.this,PrenotazioniActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    @Override
    public void taskProgress(Object[] progress) {

    }

    @Override
    public void taskCancelled(int val, int size) {

    }

    class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user

            RequestData requestData = new RequestData(PrenotaActivity.this);

            Calendar calendar = Calendar.getInstance();
            calendar.set(yearS,monthS,dayS,hourOfDay,minute);

            String newDate = new SimpleDateFormat("yyyy/MM/dd HH:mm").format(calendar.getTime());
            prenotazione.setOrario(newDate);
            Gson gson = new Gson();
            String json = gson.toJson(prenotazione);
            requestData.execute("put", Common.urlApi + "/prenotazione", json);



        }
    }
    class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            DatePickerDialog dialog= new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            yearS= year;
            monthS = month;
            dayS = day;
            DialogFragment newFragment = new TimePickerFragment();

            newFragment.show(getSupportFragmentManager(), "timePicker");
        }

    }
}
