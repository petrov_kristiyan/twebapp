package carfora.matteo.tweb.model;

public class Prenota {
    private Pizza pizza;
    private int quantità;

    public Prenota() {
        int quantità = 0;
    }

    public Prenota(Pizza pizza, int quantità) {
        this.pizza = pizza;
        this.quantità = quantità;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public int getQuantità() {
        return quantità;
    }

    public void setQuantità(int quantità) {
        this.quantità = quantità;
    }

    public void incrementaQuantità(){
        quantità++;
    }
    public void decrementaQuantità(){
        quantità--;
    }
}
