package carfora.matteo.tweb.asynctask;

import android.os.AsyncTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import carfora.matteo.tweb.Common;
import carfora.matteo.tweb.ICallBack;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RequestData extends AsyncTask<String, String, String> {

    private ICallBack<String, String> callBack;
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public RequestData(ICallBack<String, String> callBack) {
        this.callBack = callBack;
    }

    @Override
    protected String doInBackground(String... data) {
        try {
            if (data[0].equals("get"))
                return get(data[1]);
            else if (data[0].equals("put"))
                return put(data[1], data[2]);
            else if (data[0].equals("delete"))
                return delete(data[1], data[2]);
            else
                return post(data[1], data[2]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    String delete(String url, String json) throws IOException {
        OkHttpClient client = setClient();
        String cookie = Common.cookies.get(0).toString();
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(url)
                .addHeader(0 + "", cookie)
                .delete(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.message();
    }
    String put(String url, String json) throws IOException {
        OkHttpClient client = setClient();
        RequestBody body = RequestBody.create(JSON, json);
        String cookie = Common.cookies.get(0).toString();
        Request request = new Request.Builder()
                .url(url)
                .addHeader(0 + "", cookie)
                .put(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.message();
    }

    String post(String url, String json) throws IOException {
        OkHttpClient client = setClient();
        RequestBody formBody = new FormBody.Builder()
                .add("data", json)
                .build();
        Request request = new Request.Builder()
                .url(url)
                .post(formBody)
                .build();
        Response response = client.newCall(request).execute();
        return response.message();
    }

    private String get(String url) throws IOException {
        OkHttpClient client = setClient();
        String cookie = Common.cookies.get(0).toString();
        Request request = new Request.Builder().url(url).addHeader(0 + "", cookie).build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }


    private OkHttpClient setClient() {
        return new OkHttpClient.Builder()
                .cookieJar(new CookieJar() {
                    @Override
                    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
                        Common.cookies = cookies;
                    }

                    @Override
                    public List<Cookie> loadForRequest(HttpUrl url) {
                        if (Common.cookies != null)
                            return Common.cookies;
                        return new ArrayList<>();
                    }
                }).build();
    }

    @Override
    protected void onProgressUpdate(String... values) {
        callBack.taskProgress(values);
    }


    @Override
    protected void onPostExecute(String s) {
        callBack.taskCompleted(s);
    }
}