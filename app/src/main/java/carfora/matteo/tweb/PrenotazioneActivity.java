package carfora.matteo.tweb;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.Map;

import carfora.matteo.tweb.asynctask.RequestData;
import carfora.matteo.tweb.model.Prenotazione;
import carfora.matteo.tweb.model.User;

public class PrenotazioneActivity extends AppCompatActivity implements ICallBack {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prenotazione);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Prenotazione prenotazione = (Prenotazione) getIntent().getSerializableExtra("prenotazione");

        TextView orario = (TextView) findViewById(R.id.orario);
        TextView consegnata = (TextView) findViewById(R.id.consegnata);
        TextView listaPizze = (TextView) findViewById(R.id.listaPizze);
        TextView listaOrari = (TextView) findViewById(R.id.listaOrari);

        orario.setText(prenotazione.getOrario());
        consegnata.setText(prenotazione.isConsegna()==false?"NO":"SI");
        String listaP = "", listaO ="";
        for (Map.Entry<String, Integer> entry : prenotazione.getPizze().entrySet()) {
            listaP += entry.getKey() +"\n";
            listaO += entry.getValue()+"\n";
        }
        listaPizze.setText(listaP);
        listaOrari.setText(listaO);
        Button consegna = (Button) findViewById(R.id.consegna);

        consegna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestData requestData = new RequestData(PrenotazioneActivity.this);
                Gson gson = new Gson();
                prenotazione.setConsegna(true);

                String json = gson.toJson(prenotazione);
                requestData.execute("post", Common.urlApi+"/prenotazione", json);
            }
        });

        Button cancella = (Button) findViewById(R.id.cancella);
        cancella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestData requestData = new RequestData(PrenotazioneActivity.this);
                Gson gson = new Gson();
                String json = gson.toJson(prenotazione);
                requestData.execute("delete", Common.urlApi + "/prenotazione", json);
            }
        });

        if(prenotazione.isConsegna()){
            consegna.setVisibility(View.GONE);
            cancella.setVisibility(View.GONE);
        }
    }

    @Override
    public void taskStart() {

    }

    @Override
    public void taskCompleted(Object result) {
        if(result.equals("OK")){
            Toast.makeText(getApplicationContext(), "Operazione eseguita con successo!!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(PrenotazioneActivity.this,PrenotazioniActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        else {
            Toast.makeText(getApplicationContext(), "Operazione fallita", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void taskProgress(Object[] progress) {

    }

    @Override
    public void taskCancelled(int val, int size) {

    }
}
