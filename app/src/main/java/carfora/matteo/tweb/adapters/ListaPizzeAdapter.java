package carfora.matteo.tweb.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import carfora.matteo.tweb.R;
import carfora.matteo.tweb.model.Pizza;

public class ListaPizzeAdapter extends ArrayAdapter<Pizza> {

    public ListaPizzeAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public ListaPizzeAdapter(Context context, ArrayList<Pizza> items) {
        super(context, -1, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.pizza, null);
        }

        Pizza pizza = getItem(position);
        TextView nome = (TextView) v.findViewById(R.id.nome);
        TextView ingredienti = (TextView) v.findViewById(R.id.ingredienti);
        TextView prezzo = (TextView) v.findViewById(R.id.prezzo);

        nome.setText(pizza.getNome());
        ingredienti.setText(pizza.getIngredienti());
        prezzo.setText("€ "+ pizza.getPrezzo());


        return v;
    }

}
