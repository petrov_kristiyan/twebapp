package carfora.matteo.tweb.adapters;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import carfora.matteo.tweb.PrenotazioneActivity;
import carfora.matteo.tweb.R;
import carfora.matteo.tweb.model.Prenotazione;

public class ListaPrenotazioniAdapter extends ArrayAdapter<Prenotazione> {

    public ListaPrenotazioniAdapter(Context context, ArrayList<Prenotazione> items) {
        super(context, -1, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.prenotazione, null);
        }

        Prenotazione prenotazione = getItem(position);
        TextView orario = (TextView) v.findViewById(R.id.orario);
        TextView consegna = (TextView) v.findViewById(R.id.consegna);

        orario.setText(prenotazione.getOrario());
        if(!prenotazione.isConsegna()){
            consegna.setVisibility(View.GONE);
        }
        else{
            consegna.setVisibility(View.VISIBLE);

        }

        return v;
    }
}
