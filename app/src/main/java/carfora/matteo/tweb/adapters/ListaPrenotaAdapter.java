package carfora.matteo.tweb.adapters;


import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import carfora.matteo.tweb.R;
import carfora.matteo.tweb.model.Pizza;
import carfora.matteo.tweb.model.Prenota;
import carfora.matteo.tweb.model.Prenotazione;

public class ListaPrenotaAdapter extends ArrayAdapter<Prenota> {

    public ListaPrenotaAdapter(Context context, ArrayList<Prenota> items) {
        super(context, -1, items);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.prenota, null);
        }
        final Prenota prenota = getItem(position);
        TextView nome = (TextView) v.findViewById(R.id.nome);
        TextView ingredienti = (TextView) v.findViewById(R.id.ingredienti);
        TextView prezzo = (TextView) v.findViewById(R.id.prezzo);
        final TextView quantità = (TextView) v.findViewById(R.id.quantità);
        nome.setText(prenota.getPizza().getNome());
        ingredienti.setText(prenota.getPizza().getIngredienti());
        prezzo.setText("€ " + prenota.getPizza().getPrezzo());
        quantità.setText(prenota.getQuantità() + "");
        Button add = (Button) v.findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prenota.incrementaQuantità();
                quantità.setText(prenota.getQuantità() + "");
            }
        });
        Button decr = (Button) v.findViewById(R.id.decr);
        decr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(prenota.getQuantità() > 0) {
                    prenota.decrementaQuantità();
                    quantità.setText(prenota.getQuantità() + "");
                }
            }
        });
        return v;
    }
}
